﻿using System;
using System.Threading;
using Iridescence.Input;
using Iridescence.Input.DirectInput;
using Iridescence.Input.OpenTK;

namespace GraphicsDemo
{
	public static class InputTest
	{
		public static void Main()
		{
			var tk = new TKInputSystem();
			var di = new DIInputSystem();

			var tkKb = tk.GetDefaultKeyboard();
			var tkMouse = tk.GetDefaultMouse();
			tkKb.KeyDown += (s, e) => { Console.WriteLine("TK + {0}", e.Key); };
			tkKb.KeyUp += (s, e) => { Console.WriteLine("TK - {0}", e.Key); };
			tkMouse.Moved += (s, e) => { Console.WriteLine("TM * {0} {1} {2}", e.X, e.Y, e.Wheel); };
			tkMouse.ButtonDown += (s, e) => { Console.WriteLine("TM + {0}", e.Button); };
			tkMouse.ButtonUp += (s, e) => { Console.WriteLine("TM - {0}", e.Button); };

			var diKb = di.GetDefaultKeyboard();
			var diMouse = di.GetDefaultMouse();
			diKb.KeyDown += (s, e) => { Console.WriteLine("DK + {0}", e.Key); };
			diKb.KeyUp += (s, e) => { Console.WriteLine("DK - {0}", e.Key); };
			diMouse.Moved += (s, e) => { Console.WriteLine("DM * {0} {1} {2}", e.X, e.Y, e.Wheel); };
			diMouse.ButtonDown += (s, e) => { Console.WriteLine("DM + {0}", e.Button); };
			diMouse.ButtonUp += (s, e) => { Console.WriteLine("DM - {0}", e.Button); };

			while (true)
			{
				tkKb.Update();
				tkMouse.Update();
				diKb.Update();
				diMouse.Update();
				Thread.Sleep(10);
			}
		}
	}
}
