﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Iridescence;
using Iridescence.FileSystem;
using Iridescence.Graphics;
using Iridescence.Graphics.Font;
using Iridescence.Graphics.Font.FreeType;
using Iridescence.Graphics.IO;
using Iridescence.Graphics.OpenGL;
using Iridescence.Input;
using Iridescence.Input.OpenTK;
using Iridescence.Math;
using Iridescence.Physics2D.Collision;
using Iridescence.Physics2D.Collision.Shapes;
using Iridescence.Physics2D.Dynamics;
using Iridescence.Physics2D.Dynamics.Contacts;
using Iridescence.Physics2D.Dynamics.Controllers;
using Iridescence.Physics2D.Dynamics.Joints;
using Color4 = Iridescence.Math.Color4;
using Vector2 = Iridescence.Math.Vector2;
using Vector4 = Iridescence.Math.Vector4;

namespace GraphicsDemo
{
	public static class Demo
	{
		private static OpenTK.GameWindow window;
		private static Context context;
		private static bool running;
		private static ManualResetEvent exitEvent;

		public static void Main(string[] args)
		{
			Trace.Listeners.Add(new DebugTraceListener());
			running = true;

			ManualResetEvent ev = new ManualResetEvent(false);

			Thread windowThread = new Thread(() =>
			{
				window = new OpenTK.GameWindow(640, 480, OpenTK.Graphics.GraphicsMode.Default, "memes", OpenTK.GameWindowFlags.Default, OpenTK.DisplayDevice.Default, 3, 3, OpenTK.Graphics.GraphicsContextFlags.Debug | OpenTK.Graphics.GraphicsContextFlags.ForwardCompatible);
				window.Context.MakeCurrent(null);

				Thread thread = new Thread(() =>
				{
					window.MakeCurrent();

					context = new Context();
					context.Run(ev);
				});
				thread.Name = "GL thread";
				thread.Start();

				window.Run(60.0);
			});
			windowThread.Name = "Window thread";
			windowThread.Start();

			ev.WaitOne();

			window.Closing += onClosing;

			exitEvent = new ManualResetEvent(false);
			run();

			context.Stop();

			window.Close();
			window = null;
			
			GC.Collect();
			GC.WaitForPendingFinalizers();
		}

		private static void onClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			running = false;
			exitEvent.WaitOne();
		}

		private static string findResources()
		{
			DirectoryInfo dir = new DirectoryInfo(".");
			while (dir.Parent != null)
			{
				string resPath = Path.Combine(dir.FullName, "Resources");
				if (Directory.Exists(resPath))
					return resPath;

				dir = dir.Parent;
			}

			return null;
		}

		private static void run()
		{
			//InputSystem input = new TKInputSystem();
			//Mouse mouse = input.GetDefaultMouse();

			FileSystem fs = new FileSystem();
			fs.Mount("/", findResources());
			fs.DefaultContext.AddHandlers(
				new TextResource(),
				new ShaderResource(context),
				new PortableNetworkGraphicsResource(),
				new TargaResource()
				/*new FreeTypeFaceResource(new FreeTypeLibrary())*/);
			
			World world = new World();
			world.AddController(new GravityControllerDescriptor(world, new Vector2(0.0f, -10.0f)));
			Body bodyA, bodyB;

			bodyA = world.AddBody(new BodyDescriptor(BodyType.Static));
			bodyA.AddFixture(new FixtureDescriptor(new ChainShape(new[]
			{
				new Vector2(-3.5f, 3.5f), 
				new Vector2(-3.5f, -3.5f),
				new Vector2(3.5f, -2.5f),
				new Vector2(3.5f, 3.5f)
			}, true), 0.0f));

			const int pyramidBase = 15;
			const float radius = 0.1f;
			Shape shape = PolygonShape.CreateBox(radius, radius);
			for (int i = 0; i < pyramidBase; i++)
			{
				for (int j = 0; j < i; j++)
				{
					bodyA = world.AddBody(new BodyDescriptor(BodyType.Dynamic, new Vector2(((-i * 0.5f) + j) * 2.0f * (radius + shape.Radius), 1.0f + i * -2.0f * (radius + shape.Radius)), 0.0f));
					bodyA.AddFixture(new FixtureDescriptor(shape, 1.0f) { Restitution = 0f });
				}
			} 
			
			bodyA = world.AddBody(new BodyDescriptor(BodyType.Static, new Vector2(0.0f, 2.5f), 0.0f));
			bodyA.AddFixture(new FixtureDescriptor(PolygonShape.CreateBox(2.0f, 0.1f), 1.0f));

			const int numBalls = 8;
			const float ballRadius = 0.2f;
			const float ropeLength = 1.5f;
			shape = new CircleShape(ballRadius);
			for (int i = 0; i < numBalls; i++)
			{
				Vector2 anchor = new Vector2((i - numBalls * 0.5f + 0.5f) * (2.0f * ballRadius), 0.0f);
				Vector2 pos = new Vector2(anchor.X, bodyA.Position.Y - ropeLength);

				bodyB = world.AddBody(new BodyDescriptor(BodyType.Dynamic, pos, 0.0f));
				bodyB.AddFixture(new FixtureDescriptor(shape, 1.0f) { Restitution = 0.99f});

				world.AddJoint(new DistanceJointDescriptor
				{
					BodyA = bodyA,
					BodyB = bodyB,
					CollideConnected = true,
					LocalAnchorA = anchor,
					LocalAnchorB = Vector2.Zero,
					Length = ropeLength,
					DampingRatio = 0.0f,
				});
			}

			Matrix4x4 projectionMatrix = Matrix4x4.Identity;
			Matrix4x4 viewMatrix = Matrix4x4.CreateTranslation(0.0f, 0.0f, 0.0f);

			var renderProgram = new Program(context);
			renderProgram.Attach(fs.DefaultContext.Read<VertexShader>("/Shaders/GLSL/Vertex/MVPTransform.glsl"));
			renderProgram.Attach(fs.DefaultContext.Read<FragmentShader>("/Shaders/GLSL/Fragment/UniformColored.glsl"));
			renderProgram.Link();
			var renderProgramState = new ProgramState(renderProgram);
			
			MouseJoint mouseJoint = null;
			Action createCommandList = null;
			
			List<IPhysicsRenderer> renderers = new List<IPhysicsRenderer>();

			// body renderers.
			foreach (Body b in world.Bodies) renderers.Add(new BodyRenderer(context, b, renderProgramState));
			world.BodyAdded += (sender, args) => { lock (renderers) renderers.Add(new BodyRenderer(context, args.Body, renderProgramState)); };
			world.BodyRemoved += (sender, args) => { lock (renderers) renderers.RemoveAll(renderer => ((renderer as BodyRenderer)?.Body == args.Body)); };

			// joint renderers.
			foreach (Joint j in world.Joints) renderers.Add(new JointRenderer(context, j, renderProgramState));
			world.JointAdded += (sender, args) => { lock (renderers) renderers.Add(new JointRenderer(context, args.Joint, renderProgramState)); };
			world.JointRemoved += (sender, args) => { lock (renderers) renderers.RemoveAll(renderer => ((renderer as JointRenderer)?.Joint == args.Joint)); };


			Func<float, float, Vector2> windowToWorld = (x, y) =>
			{
				Matrix4x4 projInv;
				Matrix4x4.Invert(ref projectionMatrix, out projInv);

				Vector4 mousePosition = Vector4.UnitW;
				mousePosition.X = (x / window.Width) * 2.0f - 1.0f;
				mousePosition.Y = 1.0f - (y / window.Height) * 2.0f;
				Matrix4x4.Multiply(ref projInv, ref mousePosition, out mousePosition);
				return mousePosition.XY;
			};

			window.MouseDown += (s, e) =>
			{
				if (e.Button != OpenTK.Input.MouseButton.Left)
					return;

				if (mouseJoint != null)
					return;

				Vector2 p = windowToWorld(e.X, e.Y);
				AABB aabb;
				aabb.LowerBound = p;
				aabb.UpperBound = p;

				Body bodyUnderMouse = null;
				world.QueryAABB((a) =>
				{
					if (a.TestPoint(p))
					{
						bodyUnderMouse = a.Body;
						return false;	
					}
					return true;
			 	}, aabb);

				if (bodyUnderMouse != null)
				{
					MouseJointDescriptor mouseJointDef = new MouseJointDescriptor();
					mouseJointDef.BodyA = bodyUnderMouse;
					mouseJointDef.BodyB = bodyUnderMouse;
					mouseJointDef.Target = p;
					mouseJointDef.MaxForce = 1000.0f * bodyUnderMouse.Mass;
					
					lock (world)
						mouseJoint = (MouseJoint)world.AddJoint(mouseJointDef);
									}
			};

			window.MouseUp += (s, e) =>
			{
				if (mouseJoint == null)
					return;

				lock (world)
					world.RemoveJoint(mouseJoint);
			
				mouseJoint = null;
			};

			window.MouseMove += (s, e) =>
			{
				if (mouseJoint == null)
					return;

				mouseJoint.Target = windowToWorld(e.X, e.Y);
			};

			/*Font font = fs.DefaultContext.ReadResource<Font>("/Fonts/ProFontWindows.ttf");
			Texture2DImageProvider texFontTextures = new Texture2DImageProvider(r, new Format(FormatType.R8, FormatSemantic.UNorm, FormatChannelMapping.Red, FormatChannelMapping.Red, FormatChannelMapping.Red, FormatChannelMapping.One), 64);
			TextureFont texFont = new TextureFont(font.GetInstance(12.0f), texFontTextures);
			TextRenderer textRenderer = new TextRenderer(r, texFont, texFontTextures);
			
			var textVS = fs.DefaultContext.ReadResource<VertexShader>("/Shaders/GLSL/Font/FontVertex.glsl");
			var textGS = fs.DefaultContext.ReadResource<GeometryShader>("/Shaders/GLSL/Font/FontGeometry.glsl");
			var textFS = fs.DefaultContext.ReadResource<FragmentShader>("/Shaders/GLSL/Font/FontFragment.glsl");
			var textPipeline = TextRenderer.CreatePipeline(textVS, textGS, textFS);
			*/

			//textRenderer.Write(8, 8, "This has been an experiment. Hopefully, it will have been worth the weight.");

			//for (int i = 0; i < texFontTextures.Count; i++)
			//	fs.DefaultContext.WriteResource(string.Format("/Temp/TexFont-{0}.tga", i), texFontTextures.GetTexture(i));

			context.State.Framebuffer.ClearColor = SolarizedColors.Base03;

			float t = 0.0f;
			Stopwatch watch = new Stopwatch();
			Stopwatch physWatch = new Stopwatch();
			while (running)
			{
				float dt = (float)watch.Elapsed.TotalSeconds;
				t += dt;
				
				watch.Restart();

				physWatch.Restart();
				lock (world)
					world.Step(dt, 64, 4);
				physWatch.Stop();

				float aspect = (float)window.Width / window.Height;
				const float size = 4.0f;
				projectionMatrix = Matrix4x4.CreateOrthographic(-size * aspect, size * aspect, -size, size, -1.0f, 1.0f);

				renderProgramState.Set("ProjectionMatrix", projectionMatrix);
				renderProgramState.Set("ViewMatrix", viewMatrix);

				//Console.WriteLine("{0:0.0}", physWatch.Elapsed.TotalMilliseconds);

				context.State.Transformation.Viewport = new RectangleI(0, 0, window.ClientSize.Width, window.ClientSize.Height);
				context.Clear(true, true, false);

				lock (renderers)
				{
					foreach (IPhysicsRenderer renderer in renderers)
					{
						renderer.Draw();
					}
				}
				
				context.Invoke(window.SwapBuffers);
				
				//mouse.Update();
				//while (mouse[MouseButton.Right])
				//{
				//	mouse.Update();
				//}

				watch.Stop();
			}

			exitEvent.Set();
		}
	}
}
