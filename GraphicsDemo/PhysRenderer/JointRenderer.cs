﻿using System.Collections.Generic;
using Iridescence.Graphics;
using Iridescence.Graphics.OpenGL;
using Iridescence.Math;
using Iridescence.Physics2D.Dynamics.Joints;
using OpenTK.Graphics.OpenGL;

namespace GraphicsDemo
{
	/// <summary>
	/// Renders physics joints.
	/// </summary>
	internal sealed class JointRenderer : IPhysicsRenderer
	{
		#region Fields

		private readonly Context context;
		private readonly ProgramState programState;

		private readonly Buffer positionBuffer;
		private readonly Vector2[] vertices;
		private readonly VertexArray vertexArray;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the joint that is rendered.
		/// </summary>
		public Joint Joint { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new JointRenderer.
		/// </summary>
		public JointRenderer(Context context, Joint joint, ProgramState programState)
		{
			this.context = context;
			this.programState = programState.CreateDerivedState();
			this.programState.Set("Color", SolarizedColors.Red);
			this.programState.Set("ModelMatrix", Matrix4x4.Identity);
			this.Joint = joint;

			this.vertices = new Vector2[2];
			this.positionBuffer = Buffer.Create(context, this.vertices);
			this.vertexArray = new VertexArray(context);
			this.vertexArray.Bind(0, this.positionBuffer, new Format(FormatType.R32G32, FormatSemantic.Float), 8, 0);
		}

		#endregion

		#region Methods

		public void Draw()
		{
			this.vertices[0] = this.Joint.AnchorA;
			this.vertices[1] = this.Joint.AnchorB;
			this.positionBuffer.Set(this.vertices);

			this.context.Draw(this.programState, this.vertexArray, new ArrayDrawInfo(PrimitiveType.Lines, 0, 2));
		}

		#endregion
	}
}
