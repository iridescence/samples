﻿using System;
using System.Collections.Generic;
using Iridescence.Graphics;
using Iridescence.Graphics.OpenGL;
using Iridescence.Math;
using Iridescence.Physics2D.Collision.Shapes;
using Iridescence.Physics2D.Dynamics;
using OpenTK.Graphics.OpenGL;
using Buffer = Iridescence.Graphics.OpenGL.Buffer;

namespace GraphicsDemo
{
	/// <summary>
	/// Renders physics bodies.
	/// </summary>
	internal sealed class BodyRenderer : IPhysicsRenderer
	{
		#region Fields

		private readonly Context context;
		private readonly ProgramState programState;
	
		private Buffer positionBuffer;

		private Buffer indexBufferSolid;
		private int numSolidVertices;

		private Buffer indexBufferOutline;
		private int numOutlineVertices;

		private VertexArray vertexArray;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the body that is rendered.
		/// </summary>
		public Body Body { get; }

		#endregion

		#region Constructors

		public BodyRenderer(Context context, Body body, ProgramState programState)
		{
			this.context = context;
			this.Body = body;
			this.programState = programState.CreateDerivedState();

			this.initialize();
		}

		#endregion

		#region Methods

		private void initialize()
		{
			List<Vector2> positions = new List<Vector2>();
			List<int> solidIndices = new List<int>();
			List<int> outlineIndices = new List<int>();

			foreach (Fixture fixture in this.Body.Fixtures)
				this.generateVertices(positions, solidIndices, outlineIndices, fixture);

			if (positions.Count > 0)
			{
				this.positionBuffer = Buffer.Create(this.context, positions.ToArray());
				this.vertexArray = new VertexArray(this.context);
				this.vertexArray.Bind(0, this.positionBuffer, new Format(FormatType.R32G32, FormatSemantic.Float), 8, 0);

				if (solidIndices.Count > 0)
					this.indexBufferSolid = Buffer.Create(this.context, solidIndices.ToArray());

				if (outlineIndices.Count > 0)
					this.indexBufferOutline = Buffer.Create(this.context, outlineIndices.ToArray());
			}

			this.numSolidVertices = solidIndices.Count;
			this.numOutlineVertices = outlineIndices.Count;
		}

		public void Draw()
		{
			Matrix4x4 matrix = Matrix4x4.Identity;
			
			matrix.M11 = this.Body.Transform.Q.C;
			matrix.M12 = -this.Body.Transform.Q.S;
			matrix.M21 = this.Body.Transform.Q.S;
			matrix.M22 = this.Body.Transform.Q.C;
			matrix.M14 = this.Body.Transform.P.X;
			matrix.M24 = this.Body.Transform.P.Y;

			Color4 solidColor;
			Color4 outlineColor;

			if (this.Body.Awake)
			{
				solidColor = SolarizedColors.Base02;
				switch (this.Body.Type)
				{
					case BodyType.Dynamic:
						outlineColor = SolarizedColors.Green;
						break;

					case BodyType.Kinematic:
						outlineColor = SolarizedColors.Orange;
						break;

					default:
						outlineColor = SolarizedColors.Blue;
						break;
				}
			}
			else
			{
				solidColor = SolarizedColors.Base01;
				outlineColor = SolarizedColors.Base00;
			}
			
			this.programState.Set("ModelMatrix", matrix);
			
			this.programState.Set("Color", solidColor);

			if (this.indexBufferSolid != null)
				this.context.Draw(this.programState, this.vertexArray, new IndexedDrawInfo(PrimitiveType.Triangles, this.indexBufferSolid, DrawElementsType.UnsignedInt, 0, this.numSolidVertices));

			this.programState.Set("Color", outlineColor);

			if(this.indexBufferOutline != null)
				this.context.Draw(this.programState, this.vertexArray, new IndexedDrawInfo(PrimitiveType.Lines, this.indexBufferOutline, DrawElementsType.UnsignedInt, 0, this.numOutlineVertices));
		}


		private void generateVertices(List<Vector2> positions, List<int> solidIndices, List<int> outlineIndices, Fixture fixture)
		{
			Shape shape = fixture.Shape;
			if (shape is CircleShape)
			{
				CircleShape circle = (CircleShape)shape;
				int subdivisions = Math.Max(4, (int)(Utility.Pi2 * circle.Radius * 16.0f));
				float angle = Utility.Pi2 / subdivisions;

				Vector2 center = new Vector2(circle.Position.X, circle.Position.Y);
				int centerIndex = positions.Count;
				positions.Add(center);

				for (int i = 0; i <= subdivisions; i++)
				{
					Vector2 current = new Vector2(Utility.Cos(i * angle), Utility.Sin(i * angle)) * circle.Radius;
					positions.Add(current);
					if (i > 0)
					{
						solidIndices.Add(centerIndex);
						solidIndices.Add(centerIndex + i);
						solidIndices.Add(centerIndex + i + 1);

						outlineIndices.Add(centerIndex + i);
						outlineIndices.Add(centerIndex + i + 1);
					}
				}

				outlineIndices.Add(centerIndex);
				outlineIndices.Add(centerIndex + 1);
			}
			else if (shape is PolygonShape)
			{
				PolygonShape polygon = (PolygonShape)shape;
				Vector2[] vertices = polygon.GetVertices();

				int firstIndex = positions.Count;
				for (int i = 0; i < polygon.VertexCount; i++)
				{
					positions.Add(new Vector2(vertices[i].X, vertices[i].Y));
					if (i > 0)
						outlineIndices.Add(firstIndex + i - 1);
					else
						outlineIndices.Add(firstIndex + polygon.VertexCount - 1);

					outlineIndices.Add(firstIndex + i);	
				}

				for (int i = 2; i < polygon.VertexCount; i++)
				{
					solidIndices.Add(firstIndex);
					solidIndices.Add(firstIndex + i - 1);
					solidIndices.Add(firstIndex + i);
				}
			}
			else if (shape is EdgeShape)
			{
				EdgeShape edge = (EdgeShape)shape;

				positions.Add(edge.Vertex1);
				positions.Add(edge.Vertex2);

				outlineIndices.Add(positions.Count - 2);
				outlineIndices.Add(positions.Count - 1);
			}
			else if (shape is ChainShape)
			{
				ChainShape chain = (ChainShape)shape;
				Vector2[] vertices = chain.GetVertices();

				for (int i = 0; i < vertices.Length; i++)
				{
					positions.Add(vertices[i]);
					if (i > 0)
					{
						outlineIndices.Add(positions.Count - 2);
						outlineIndices.Add(positions.Count - 1);
					}
				}
			}
			else
			{
				throw new NotSupportedException();
			}
		}
		
		#endregion
	}
}
