﻿namespace GraphicsDemo
{
	internal interface IPhysicsRenderer
	{
		void Draw();
	}
}
