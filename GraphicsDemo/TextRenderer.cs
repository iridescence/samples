﻿using System;
using System.Collections.Generic;
using Iridescence.Graphics;
using Iridescence.Graphics.Font;
using Iridescence.Math;

namespace GraphicsDemo
{
	/*
	public sealed class TextRenderer
	{
		#region Nested Types

		private const int glyphSize = 12; 

		private struct Glyph
		{
			public short X;
			public short Y;
			public short Width;
			public short Height;
			public short TexX;
			public short TexY;
		}

		private struct Page
		{
			public int Count; 
			public Glyph[] Glyphs;
			
			public Memory Memory;
		}

		#endregion

		#region Fields

		private readonly Device device;
		private readonly TextureFont font;
		private readonly Texture2DImageProvider textures;

		private Color4 color;
		private Vector2 outputSize;
		
		private bool invalid;
		private Page[] pages;
		private readonly Memory fontInfo;
		
		private static readonly Format positionFormat = new Format(FormatType.R16G16, FormatSemantic.SInt);
		private static readonly Format sizeFormat = new Format(FormatType.R16G16, FormatSemantic.SInt);
		private static readonly Format textureCoordinateFormat = new Format(FormatType.R16G16, FormatSemantic.SInt);
		private static readonly Sampler pageSampler = new Sampler(FilterMethod.PointPoint);
		
		#endregion

		#region Properties

		public Color4 Color
		{
			get { return this.color; }
			set
			{
				this.color = value;
				this.fontInfo.Write(0, value);
			}
		}

		public Vector2 OutputSize
		{
			get { return this.outputSize; }
			set
			{
				this.outputSize = value;
				this.fontInfo.Write(16, new Vector2(2.0f / value.X, 2.0f / value.Y));
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TextRenderer.
		/// </summary>
		public TextRenderer(Device device, TextureFont font, Texture2DImageProvider textures)
		{
			this.device = device;
			this.font = font;
			this.textures = textures;

			this.fontInfo = device.AllocateMemory(32);
			this.Color = Color4.White;

			this.pages = new Page[1];
		}

		#endregion

		#region Methods

		public void Clear()
		{
			for (int i = 0; i < this.pages.Length; i++)
			{
				this.pages[i].Count = 0;
			}

			this.invalid = true;
		}


		public void Write(float x, float y, string text)
		{
			FontGlyph previous = null;
			for (int i = 0; i < text.Length; i++)
			{
				char c = text[i];

				FontGlyph glyph = this.font.FontInstance.GetGlyph(text[i]);

				int pageIndex = this.font.GetPageIndex(c);
				if (pageIndex >= this.pages.Length)
					Array.Resize(ref this.pages, pageIndex + 1);

				TextureFontPage texPage = this.font.GetPage(c);
				TextureFontGlyph texGlyph = texPage.GetGlyph(c);

				if (this.pages[pageIndex].Glyphs == null)
					this.pages[pageIndex].Glyphs = new Glyph[8];
				else if (this.pages[pageIndex].Count >= this.pages[pageIndex].Glyphs.Length)
					Array.Resize(ref this.pages[pageIndex].Glyphs, this.pages[pageIndex].Glyphs.Length * 2);

				Glyph g;
				g.X = (short)(x + glyph.Offset.X);
				g.Y = (short)(y - texGlyph.Height - glyph.Offset.Y);
				g.Width = (short)texGlyph.Width;
				g.Height = (short)texGlyph.Height;
				g.TexX = (short)texGlyph.X;
				g.TexY = (short)texGlyph.Y;

				this.pages[pageIndex].Glyphs[this.pages[pageIndex].Count++] = g;
				
				x += glyph.Advance.X;
				y += glyph.Advance.Y;

				if (previous != null)
				{
					Vector2 kerning = previous.GetKerning(glyph);
					x += kerning.X;
					y += kerning.Y;
				}

				previous = glyph;
			}

			this.invalid = true;
		}

		public void Draw(List<Command> commands)
		{
			commands.BindMemory("FontInfo", this.fontInfo);
			commands.BindSampler("PageSampler", pageSampler);

			for (int i = 0; i < this.pages.Length; i++)
			{
				if (this.pages[i].Count == 0)
					continue;

				if (this.invalid)
				{
					int size = this.pages[i].Count * glyphSize;
					if (this.pages[i].Memory == null || this.pages[i].Memory.Size < size)
						this.pages[i].Memory = this.device.AllocateMemory(size);

					this.pages[i].Memory.Write(this.pages[i].Glyphs, 0, 0, this.pages[i].Count);	
				}

				commands.BindTexture("Page", this.textures.GetTexture(i));
				commands.BindMemory("Position", this.pages[i].Memory, positionFormat, 0, glyphSize);
				commands.BindMemory("Size", this.pages[i].Memory, sizeFormat, 4, glyphSize);
				commands.BindMemory("TextureCoordinate", this.pages[i].Memory, textureCoordinateFormat, 8, glyphSize);
				commands.Draw(0, this.pages[i].Count);
			}

			this.invalid = false;
		}

		public static PipelineState CreatePipeline(VertexShader vertexShader, GeometryShader geometryShader, FragmentShader fragmentShader)
		{
			RasterizerState rs = new RasterizerState(CullFace.None, FaceWinding.CounterClockwise);
			DepthStencilState ds = new DepthStencilState(false, CompareFunction.Always, false, false, StencilFace.Default, StencilFace.Default);
			BlendState blend = new BlendState(BlendFactor.SourceAlpha, BlendFactor.OneMinusSourceAlpha);
			return new PipelineState(PrimitiveType.PointList, vertexShader, geometryShader, rs, fragmentShader, ds, blend);
		}

		#endregion
	}
	*/
}
