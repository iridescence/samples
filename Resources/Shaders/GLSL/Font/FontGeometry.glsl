#version 330 core

uniform FontInfo
{
	vec4 Color;
	vec2 OutputScale;
};

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

in vec2 position[];
in vec2 size[];
in vec2 textureCoordinate[];

out vec2 texCoord;

void emit(vec4 x, vec4 y)
{
	texCoord = vec2(x.z, y.w);
	gl_Position = vec4(x.x * OutputScale.x - 1.0, y.y * OutputScale.y - 1.0, 0.0, 1.0);
	EmitVertex();
}

void main(void)
{
	vec4 v1;
	vec4 v2;
	
	v1.xy = position[0];
	v1.zw = textureCoordinate[0];
	
	v2 = v1 + size[0].xyxy;

	emit(v1, v1);
	emit(v1, v2);
	emit(v2, v1);
	emit(v2, v2);
	
	EndPrimitive();
}
