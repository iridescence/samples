#version 330 core

in vec2 Position;
in vec2 Size;
in vec2 TextureCoordinate;

out vec2 position;
out vec2 size;
out vec2 textureCoordinate;

void main(void)
{
	position = Position;
	size = Size;
	textureCoordinate = TextureCoordinate;
}