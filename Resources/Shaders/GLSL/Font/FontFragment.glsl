#version 330 core

uniform FontInfo
{
	vec4 Color;
	vec2 OutputScale;
};

uniform sampler2D Page;

in vec2 texCoord;

out vec4 outColor;

void main(void)
{
	outColor = vec4(Color.rgb, Color.a * texelFetch(Page, ivec2(texCoord), 0).r);
}
