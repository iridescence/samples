#version 330

uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;

layout(location = 0) in vec3 Position;

void main()
{
	vec4 pos = vec4(Position, 1.0);
	gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * pos;
}