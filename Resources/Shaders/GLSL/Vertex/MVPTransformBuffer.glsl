#version 330

layout(row_major) uniform Scene
{
	mat4 ProjectionMatrix;
	mat4 ViewMatrix;
};

layout(row_major) uniform Model
{
	mat4 ModelMatrix;
};

in vec3 Position;

void main()
{
	vec4 pos = vec4(Position, 1.0) + vec4(0.0, 0.0, float(gl_InstanceID) * 0.5, 0.0);
	gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * pos;
}