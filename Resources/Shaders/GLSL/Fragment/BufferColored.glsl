#version 330

uniform Color
{
	vec4 color;
};

out vec4 outColor;

void main()
{
	outColor = color;
}