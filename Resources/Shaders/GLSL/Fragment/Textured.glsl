#version 330

uniform sampler2D ColorTexture;

in vec2 texCoord;

out vec4 outColor;

void main()
{
	outColor = texture(ColorTexture, texCoord);
}