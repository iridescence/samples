#version 330

uniform vec4 Color;

out vec4 outColor;

void main()
{
	outColor = Color;
}