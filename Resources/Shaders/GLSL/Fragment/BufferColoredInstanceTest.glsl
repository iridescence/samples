#version 330

uniform Color
{
	vec4 color[6];
};

out vec4 outColor;
flat in int instanceID;

void main()
{
	outColor = color[instanceID % 6];
}