﻿using System.Collections.Generic;
using Iridescence.Graphics;

namespace ColladaDemo
{
	public class Mesh
	{
		private IEnumerable<Command> drawCommand;
		public IEnumerable<Command> DrawCommand
		{
			get { return this.drawCommand; }
		}

		public Mesh(IEnumerable<Command> drawCommand)
		{
			this.drawCommand = drawCommand;
		}
	}
}

