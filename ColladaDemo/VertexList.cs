﻿using System.Collections;
using System.Collections.Generic;

using Iridescence.Graphics;

namespace ColladaDemo
{
	public class VertexList : IEnumerable<Vertex>
	{

		#region Fields

		private readonly List<Vertex> vertices;
		private readonly int maxDepth;

		#endregion

		#region Properties

		public Vertex this[int index]
		{
			get { return this.vertices[index]; }
		}

		public int Count
		{
			get { return this.vertices.Count; }
		}

		#endregion

		#region Constructors

		public VertexList()
			: this(1024)
		{
		}

		public VertexList(int maxDepth)
		{
			this.vertices = new List<Vertex>();
			this.maxDepth = maxDepth;
		}

		#endregion

		#region Methods

		public int Add(Vertex vertex)
		{
			int lower = this.vertices.Count - this.maxDepth;
			if (lower < 0) lower = 0;
			for (int i = this.vertices.Count - 1; i > lower; i--)
				if (vertex.Compare(this.vertices[i]))
					return i;

			this.vertices.Add(vertex);
			return this.vertices.Count - 1;
		}

		public void Write(Buffer positions, Buffer normals, Buffer texCoords, Buffer colors)
		{
			List<float> pos = new List<float>();
			List<float> norm = new List<float>();
			List<float> texCoord = new List<float>();
			List<float> col = new List<float>();

			foreach(Vertex v in this.vertices)
			{
				pos.AddRange(v.Position.ToArray());
				norm.AddRange(v.Normal.ToArray());
				texCoord.AddRange(v.TextureCoordinate.ToArray());
				col.AddRange(v.Color.ToArray());
			}
			positions.Write(pos.ToArray());
			normals.Write(norm.ToArray());
			texCoords.Write(texCoord.ToArray());
			colors.Write(col.ToArray());
		}

		public IEnumerator<Vertex> GetEnumerator()
		{
			return this.vertices.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.vertices.GetEnumerator();
		}

		public Vertex[] ToArray()
		{
			return this.vertices.ToArray();
		}

		#endregion
	}
}

