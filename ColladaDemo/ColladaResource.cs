﻿using System;
using System.IO;
using System.Collections.Generic;

using Iridescence;
using Iridescence.FileSystem;
using Iridescence.Graphics;
using Iridescence.Math;

namespace ColladaDemo
{
	public sealed class ColladaResource : IResourceReader<Model>, IResourceWriter<Model>
	{
		#region Nested Types

		private sealed class Node : List<Node>
		{
			public string Id;
			public string Type;
			public Node Parent;

			public bool HasCamera;
			public bool HasGeometry;
			public bool HasController;
			public bool HasLight;

			public Matrix4x4 Transform;


			public Matrix4x4 WorldTransform
			{
				get 
				{
					if(this.Parent == null)
						return this.Transform;
					return this.Parent.WorldTransform * this.Transform;
				}
			}

			public Node()
			{
				
			}
				

		}

		private sealed class Source
		{
			public string Id;
			public Array Values;
			public int Stride;
			public int Count;
			public List<string> Parameters;

			public Source()
			{
				this.Parameters = new List<string>();
			}

			public float[] GetFloats(int index)
			{
				float[] floatValues = (float[])this.Values;
				float[] floats = new float[this.Stride];
				for(int i = 0; i < this.Stride; i++)
					floats[i] = floatValues[index * this.Stride + i];

				return floats;
			}

			public int[] getInts(int index)
			{
				int[] intValues = (int[])this.Values;
				int[] ints = new int[this.Stride];
				for(int i = 0; i < this.Stride; i++)
					ints[i] = intValues[index * this.Stride + i];

				return ints;
			}

			public string[] GetStrings(int index)
			{
				string[] stringValues = (string[])this.Values;
				string[] strings = new string[this.Stride];
				for(int i = 0; i < this.Stride; i++)
					strings[i] = stringValues[index * this.Stride + i];

				return strings;
			}
		}

		private sealed class Input
		{
			public string Semantic;
			public int Offset;
			public Source Source;
		}

		private sealed class InputCollection
		{
			private List<Input> inputs;

			public Input this[int i]
			{
				get { return this.inputs[i]; }
			}

			public int Count
			{
				get { return this.inputs.Count; }
			}

			public int MaxOffset
			{
				get { return this[this.Count - 1].Offset; }
			}

			public InputCollection()
			{
				this.inputs = new List<Input>();
			}

			public Input GetInput(string semantic)
			{
				foreach(Input input in this.inputs)
					if(input.Semantic == semantic)
						return input;
				return null;
			}

			public void Add(Input input)
			{
				for(int i = 0; i < this.inputs.Count; i++)
				{
					if(this.inputs[i].Offset > input.Offset)
					{
						this.inputs.Insert(i, input);
						return;
					}
				}
				this.inputs.Add(input);
			}

		}
		#endregion

		#region Methods

		public int Probe(FileContext context, string name)
		{
			if(name.EndsWith(".dae"))
				return 1;
			return -1;
		}

		public Model Read(FileContext context, string name)
		{
			IFile file = context.GetFile(name);
			if(file == null)
				throw new FileNotFoundException();

			using (StreamReader reader = new StreamReader(file.OpenRead()))
				return parseRootNode(context, DataNode.ReadXml(reader));				
		}

		public void Write(FileContext context, string name, Model resource)
		{
			throw new NotImplementedException();
		}

		private Model parseRootNode(FileContext context, DataNode root)
		{
			if(root.Name.ToLower() != "collada")
				throw new FileLoadException();

			if(root.GetAttribute("version") != "1.5.0")
				throw new FileLoadException("Wrong version. Expected \"1.5.0\"");

			DataNode scene = root.GetElement("scene");
			if(scene == null)
				return null;

			DataNode instanceVisualScene = scene.GetElement("instance_visual_scene");
			if(instanceVisualScene == null)
				return null;
			
			string urlVisualScene = instanceVisualScene.GetAttribute("url");
			if(urlVisualScene.StartsWith("#"))
				urlVisualScene = urlVisualScene.Substring(1);
			else
				throw new FileLoadException("Invalid url of Visual Scene.");

			DataNode libraryVisualScenes = root.GetElement("library_visual_scenes");
			if(libraryVisualScenes == null)
				throw new FileLoadException("DataNode <library_visual_scenes> not found.");

			DataNode visualScene = null;
			foreach (DataNode node in libraryVisualScenes.GetElements("visual_scene"))
			{
				if(urlVisualScene == node.GetAttribute("id", ""))
					visualScene = node;
			}

			if(visualScene == null)
				throw new FileLoadException("Visual Scene not found.");

			return parseVisualScene(context, root, visualScene);
		}

		private Model parseVisualScene(FileContext context, DataNode root, DataNode visualScene)
		{
			Model model = new Model();

			foreach (DataNode dataNode in visualScene.GetElements("node"))
			{
				Node node = parseNode(dataNode);
				foreach (DataNode instanceCamera in dataNode.GetElements("instance_camera"))
					throw new NotImplementedException();
				foreach (DataNode instanceController in dataNode.GetElements("instance_controller"))
					throw new NotImplementedException();
				foreach (DataNode instanceGeometry in dataNode.GetElements("instance_geometry"))
					model.AddRange(parseGeometry(context, root, instanceGeometry));
				foreach (DataNode instanceLight in dataNode.GetElements("instance_light"))
					throw new NotImplementedException();
				foreach (DataNode instanceNode in dataNode.GetElements("instance_node"))
					throw new NotImplementedException();
			}

			return model;
		}

		private Node parseNode(DataNode dataNode)
		{
			Node node = new Node();
			node.Id = dataNode.GetAttribute("id", "");
			node.Type = dataNode.GetAttribute("type", "NODE");
			node.HasCamera = dataNode.GetElement("instance_camera") != null;
			node.HasLight = dataNode.GetElement("instance_light") != null;
			node.HasGeometry = dataNode.GetElement("instance_geometry") != null;
			node.HasController = dataNode.GetElement("instance_controller") != null;

			int i = 0; 
			if(dataNode[i].Name == "asset")
				i = 1;

			Matrix4x4 transform = Matrix4x4.Identity;
			while(i < dataNode.Count)
			{
				if(dataNode[i].Name == "lookat")
				{
				}
				else if(dataNode[i].Name == "matrix")
					transform = new Matrix4x4(parseFloatArray(dataNode[i].Value), 0) * transform;
				else if(dataNode[i].Name == "rotate")
				{
					float[] rot = parseFloatArray(dataNode[i].Value);
					transform = Matrix4x4.CreateRotation(new Vector3(rot[0], rot[1], rot[2]), Utility.DegreesToRadians(rot[3])) * transform;
				}
				else if(dataNode[i].Name == "scale")
				{
					float[] scale = parseFloatArray(dataNode[i].Value);
					transform = Matrix4x4.CreateScale(scale[0], scale[1], scale[2]) * transform;
				}
				else if(dataNode[i].Name == "skew")
				{
				}
				else if(dataNode[i].Name == "translate")
				{
					float[] trans = parseFloatArray(dataNode[i].Value);
					transform = Matrix4x4.CreateTranslation(trans[0], trans[1], trans[2]) * transform;
				}
				else
					break;
				i++;
			}
			node.Transform = transform;

			if(dataNode.GetElement("instance_node") != null)
				throw new NotImplementedException();

			foreach(DataNode childNode in dataNode.GetElements("node"))
			{
				Node child = parseNode(childNode);
				child.Parent = node;
				node.Add(child);
			}
			return node;
		}

		private Mesh[] parseGeometry(FileContext context, DataNode root, DataNode instanceGeometry)
		{
			List<Mesh> meshes = new List<Mesh>();

			string urlGeometry = instanceGeometry.GetAttribute("url");
			if(urlGeometry.StartsWith("#"))
				urlGeometry = urlGeometry.Substring(1);
			else
				throw new FileLoadException("Invalid url of Geometry.");

			DataNode libraryGeometries = root.GetElement("library_geometries");
			if(libraryGeometries == null)
				throw new FileLoadException("DataNode <library_geometries> not found.");

			DataNode geometry = null;
			foreach (DataNode node in libraryGeometries.GetElements("geometry"))
			{
				if(urlGeometry == node.GetAttribute("id", ""))
					geometry = node;
			}

			if(geometry == null)
				throw new FileLoadException("Geometry not found.");

			if(geometry.GetElement("mesh") == null)
				throw new FileLoadException("No mesh in geometry found.");
			
			DataNode mesh = geometry.GetElement("mesh");

			Dictionary<string, Source> sources = new Dictionary<string, Source>();
			foreach(DataNode sourceNode in mesh.GetElements("source"))
			{
				Source source = parseSource(sourceNode);
				sources.Add(source.Id, source);
			}

			DataNode vertices = mesh.GetElement("vertices");

			foreach (DataNode polylist in mesh.GetElements("polylist"))
				meshes.Add(parsePolylist(context, root, vertices, sources, polylist));

			foreach(DataNode triangles in mesh.GetElements("triangles"))
				meshes.Add(parseTriangles(context, root, vertices, sources, triangles));

			return meshes.ToArray();
		}

		private Mesh parsePolylist(FileContext context, DataNode root, DataNode vertices, Dictionary<string, Source> sources, DataNode polylist)
		{
			int count = 0;
			if(!Int32.TryParse(polylist.GetAttribute("count"), out count))
				throw new FileLoadException("Polylist primitive count is not a number.");

			InputCollection inputs = new InputCollection();
			foreach(DataNode inputNode in polylist.GetElements("input"))
			{
				Input input = new Input();
				if(inputNode.GetAttribute("semantic") == "VERTEX")
				{
					if(inputNode.GetAttribute("source").Substring(1) != vertices.GetAttribute("id"))
						throw new FileLoadException("Could not find vertices for polylist");
					DataNode vertexInput = vertices.GetElement("input");
					if(!sources.TryGetValue(vertexInput.GetAttribute("source").Substring(1), out input.Source))
						throw new FileLoadException("Could not find vertex source for polylist.");

					input.Semantic = vertexInput.GetAttribute("semantic");
				}
				else if(inputNode.GetAttribute("semantic") == "NORMAL")
				{
					if(!sources.TryGetValue(inputNode.GetAttribute("source").Substring(1), out input.Source))
						throw new FileLoadException("Could not find vertex source for polylist.");
					input.Semantic = inputNode.GetAttribute("semantic");
				}
				else if(inputNode.GetAttribute("semantic") == "TEXCOORD")
				{
					if(!sources.TryGetValue(inputNode.GetAttribute("source").Substring(1), out input.Source))
						throw new FileLoadException("Could not find vertex source for polylist.");
					input.Semantic = inputNode.GetAttribute("semantic");
				}
				else if(inputNode.GetAttribute("semantic") == "COLOR")
				{
					if(!sources.TryGetValue(inputNode.GetAttribute("source").Substring(1), out input.Source))
						throw new FileLoadException("Could not find vertex source for polylist.");
					input.Semantic = inputNode.GetAttribute("semantic");
				}
				else
					continue;
				
				if(!Int32.TryParse(inputNode.GetAttribute("offset"), out input.Offset))
					throw new FileLoadException();

				inputs.Add(input);
			}

			DataNode vcountNode = polylist.GetElement("vcount");
			if(vcountNode == null)
				return null;
			int[] vcount = parseIntArray(vcountNode.Value);

			DataNode pNode = polylist.GetElement("p");
			if(pNode == null)
				return null;
			int[] p = parseIntArray(pNode.Value);

			VertexList vertexList = new VertexList();
			List<int> indices = new List<int>();

			Dictionary<string, List<float>> buffers = new Dictionary<string, List<float>>();
			for(int i = 0; i < inputs.Count; i++)
				buffers.Add(inputs[i].Semantic, new List<float>());

			int index = 0;
			for(int i = 0; i < vcount.Length; i++)
			{
				if(vcount[i] != 3)
					throw new FileLoadException("Only Triangulated meshes are supported");

				for(int j = 0; j < vcount[i]; j++)
				{
					Vertex v = new Vertex();
					for(int k = 0; k < inputs.Count; k++)
					{
						float[] floats = inputs[k].Source.GetFloats(p[index + inputs[k].Offset]);
						if(inputs[k].Semantic == "POSITION")
							v.Position = new Vector3(floats[0], floats[1], floats[2]);
						else if(inputs[k].Semantic == "NORMAL")
							v.Normal = new Vector3(floats[0], floats[1], floats[2]);
						else if(inputs[k].Semantic == "COLOR")
							v.Color = new Vector4(floats[0], floats[1], floats[2], floats[3]);
						else if(inputs[k].Semantic == "TEXCOORD")
							v.TextureCoordinate = new Vector2(floats[0], floats[1]);
					}
					indices.Add(vertexList.Add(v));

					index += inputs.MaxOffset + 1;
				}
			}

			Device r = context.GetEnv<Device>("GraphicsDevice");

			var positionBuffer = r.CreateBuffer(vertexList.Count * 12);
			var normalBuffer = r.CreateBuffer(vertexList.Count * 12);
			var texCoordBuffer = r.CreateBuffer(vertexList.Count * 8);
			var colorBuffer = r.CreateBuffer(vertexList.Count * 16);
			vertexList.Write(positionBuffer, normalBuffer, texCoordBuffer, colorBuffer);

			var indexBuffer = r.CreateBuffer(indices.Count * 4);
			indexBuffer.Write(indices.ToArray());

			PipelineState pipeline = new PipelineState(PrimitiveType.TriangleList, context.GetEnv<VertexShader>("VertexShader"), null, RasterizerState.Default, 
				context.GetEnv<FragmentShader>("FragmentShader"), DepthStencilState.Default, BlendState.Default);

			var cmdList = new List<Command>();
			cmdList.SetPipelineState(pipeline);

			cmdList.SetBuffer("Position", positionBuffer, new Format(FormatChannels.R32G32B32, FormatSemantic.Float), 0, 12);
			cmdList.SetBuffer("Normal", normalBuffer, new Format(FormatChannels.R32G32B32, FormatSemantic.Float), 0, 12);
			cmdList.SetBuffer("TexCoord", texCoordBuffer, new Format(FormatChannels.R32G32, FormatSemantic.Float), 0, 8);
			cmdList.SetBuffer("Color", colorBuffer, new Format(FormatChannels.R32G32B32A32, FormatSemantic.Float), 0, 16);

			cmdList.SetIndexBuffer(indexBuffer, IndexType.UInt32, 0);

			cmdList.DrawIndexed(0, count * 3);
			return new Mesh(cmdList);
		}

		private Mesh parseTriangles(FileContext context, DataNode root, DataNode vertices, Dictionary<string, Source> sources, DataNode triangles)
		{
			int count = 0;
			if(!Int32.TryParse(triangles.GetAttribute("count"), out count))
				throw new FileLoadException("Polylist primitive count is not a number.");

			InputCollection inputs = new InputCollection();
			foreach(DataNode inputNode in triangles.GetElements("input"))
			{
				Input input = new Input();
				if(inputNode.GetAttribute("semantic") == "VERTEX")
				{
					if(inputNode.GetAttribute("source").Substring(1) != vertices.GetAttribute("id"))
						throw new FileLoadException("Could not find vertices for polylist");
					DataNode vertexInput = vertices.GetElement("input");
					if(!sources.TryGetValue(vertexInput.GetAttribute("source").Substring(1), out input.Source))
						throw new FileLoadException("Could not find vertex source for polylist.");

					input.Semantic = vertexInput.GetAttribute("semantic");
				}
				else if(inputNode.GetAttribute("semantic") == "NORMAL")
				{
					if(!sources.TryGetValue(inputNode.GetAttribute("source").Substring(1), out input.Source))
						throw new FileLoadException("Could not find vertex source for polylist.");
					input.Semantic = inputNode.GetAttribute("semantic");
				}
				else if(inputNode.GetAttribute("semantic") == "TEXCOORD")
				{
					if(!sources.TryGetValue(inputNode.GetAttribute("source").Substring(1), out input.Source))
						throw new FileLoadException("Could not find vertex source for polylist.");
					input.Semantic = inputNode.GetAttribute("semantic");
				}
				else if(inputNode.GetAttribute("semantic") == "COLOR")
				{
					if(!sources.TryGetValue(inputNode.GetAttribute("source").Substring(1), out input.Source))
						throw new FileLoadException("Could not find vertex source for polylist.");
					input.Semantic = inputNode.GetAttribute("semantic");
				}
				else
					continue;

				if(!Int32.TryParse(inputNode.GetAttribute("offset"), out input.Offset))
					throw new FileLoadException();

				inputs.Add(input);
			}

			DataNode pNode = triangles.GetElement("p");
			if(pNode == null)
				return null;
			int[] p = parseIntArray(pNode.Value);

			VertexList vertexList = new VertexList();
			List<int> indices = new List<int>();

			Dictionary<string, List<float>> buffers = new Dictionary<string, List<float>>();
			for(int i = 0; i < inputs.Count; i++)
				buffers.Add(inputs[i].Semantic, new List<float>());

			int index = 0;
			for(int i = 0; i < count; i++)
			{
				for(int j = 0; j < 3; j++)
				{
					Vertex v = new Vertex();
					for(int k = 0; k < inputs.Count; k++)
					{
						float[] floats = inputs[k].Source.GetFloats(p[index + inputs[k].Offset]);
						if(inputs[k].Semantic == "POSITION")
							v.Position = new Vector3(floats[0], floats[1], floats[2]);
						else if(inputs[k].Semantic == "NORMAL")
							v.Normal = new Vector3(floats[0], floats[1], floats[2]);
						else if(inputs[k].Semantic == "COLOR")
							v.Color = new Vector4(floats[0], floats[1], floats[2], floats[3]);
						else if(inputs[k].Semantic == "TEXCOORD")
							v.TextureCoordinate = new Vector2(floats[0], floats[1]);
					}
					indices.Add(vertexList.Add(v));

					index += inputs.MaxOffset + 1;
				}
			}

			Device r = context.GetEnv<Device>("GraphicsDevice");

			var positionBuffer = r.CreateBuffer(vertexList.Count * 12);
			var normalBuffer = r.CreateBuffer(vertexList.Count * 12);
			var texCoordBuffer = r.CreateBuffer(vertexList.Count * 8);
			var colorBuffer = r.CreateBuffer(vertexList.Count * 16);
			vertexList.Write(positionBuffer, normalBuffer, texCoordBuffer, colorBuffer);

			var indexBuffer = r.CreateBuffer(indices.Count * 4);
			indexBuffer.Write(indices.ToArray());

			PipelineState pipeline = new PipelineState(PrimitiveType.TriangleList, context.GetEnv<VertexShader>("VertexShader"), null, RasterizerState.Default, 
				context.GetEnv<FragmentShader>("FragmentShader"), DepthStencilState.Default, BlendState.Default);

			var cmdList = new List<Command>();
			cmdList.SetPipelineState(pipeline);

			cmdList.SetBuffer("Position", positionBuffer, new Format(FormatChannels.R32G32B32, FormatSemantic.Float), 0, 12);
			cmdList.SetBuffer("Normal", normalBuffer, new Format(FormatChannels.R32G32B32, FormatSemantic.Float), 0, 12);
			cmdList.SetBuffer("TexCoord", texCoordBuffer, new Format(FormatChannels.R32G32, FormatSemantic.Float), 0, 8);
			cmdList.SetBuffer("Color", colorBuffer, new Format(FormatChannels.R32G32B32A32, FormatSemantic.Float), 0, 16);

			cmdList.SetIndexBuffer(indexBuffer, IndexType.UInt32, 0);

			cmdList.DrawIndexed(0, count * 3);
			return new Mesh(cmdList);
		}

		private Source parseSource(DataNode sourceNode)
		{
			Source source = new Source();
			source.Id = sourceNode.GetAttribute("id");
			DataNode array;
			if(sourceNode[0].Name != "asset")
				array = sourceNode[0];
			else
				array = sourceNode[1];

			if(array.Name == "float_array")
				source.Values = parseFloatArray(array.Value);
			else if(array.Name == "int_array")
				source.Values = parseIntArray(array.Value);
			else if(array.Name == "Name_array")
				source.Values = array.Value.Split(' ');
			else
				throw new FileLoadException("Unsupported array type in source.");

			DataNode techniqueCommon = sourceNode.GetElement("technique_common");
			if(techniqueCommon == null)
				throw new FileLoadException("No technique_common in source");
			DataNode accessor = techniqueCommon.GetElement("accessor");
			if(accessor == null)
				throw new FileLoadException("No accessor in technique_common.");

			if(!Int32.TryParse(accessor.GetAttribute("stride"), out source.Stride))
				throw new FileLoadException();
			if(!Int32.TryParse(accessor.GetAttribute("count"), out source.Count))
				throw new FileLoadException();

			foreach(DataNode param in accessor.GetElements("param"))
				source.Parameters.Add(param.GetAttribute("name"));

			return source;
		}

		private int[] parseIntArray(string s)
		{
			string[] str = s.Split(' ');
			int[] intArray = new int[str.Length];
			for (int i = 0; i < str.Length; i++)
				if(!Int32.TryParse(str[i], out intArray[i]))
					throw new FileLoadException("Error parsing string to int array.");
			return intArray;
		}

		private float[] parseFloatArray(string s)
		{
			string[] str = s.Split(' ');
			float[] floatArray = new float[str.Length];
			for (int i = 0; i < str.Length; i++)
				if(!Single.TryParse(str[i], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out floatArray[i]))
					throw new FileLoadException("Error parsing string to float array.");
			return floatArray;
		}

		#endregion
	}
}

