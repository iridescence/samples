﻿using System;

using Iridescence.Math;

namespace ColladaDemo
{
	public struct Vertex
	{
		#region Fields

		public Vector3 Position;
		public Vector3 Normal;
		public Vector2 TextureCoordinate;
		public Vector4 Color;

		#endregion

		#region Methods

		public bool Compare(Vertex vertex)
		{
			return this.Position == vertex.Position && 
				this.Normal == vertex.Normal && 
				this.TextureCoordinate == vertex.TextureCoordinate &&
				this.Color == vertex.Color;
		}

		#endregion
	}
}

