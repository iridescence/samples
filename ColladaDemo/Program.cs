﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Iridescence;
using Iridescence.FileSystem;
using Iridescence.Graphics;
using Iridescence.Graphics.OpenGL;
using Iridescence.Math;

namespace ColladaDemo
{
	public static class Program
	{
		private static Window window;
		private static bool running;

		public static void Main(string[] args)
		{
			Trace.Listeners.Add(new DebugTraceListener());

			running = true;

			window = new GLWindow();
			window.Closing += onClosing;

			run();

			GC.Collect();
			GC.WaitForPendingFinalizers();

			window.Dispose();
			window = null;
		}

		static void onClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = true;
			running = false;
		}

		private static string findResources()
		{
			DirectoryInfo dir = new DirectoryInfo(".");
			while (dir.Parent != null)
			{
				string resPath = Path.Combine(dir.FullName, "Resources");
				if (Directory.Exists(resPath))
					return resPath;

				dir = dir.Parent;
			}

			return null;
		}

		private static void run()
		{
			Device r = window.Device;

			FileSystem fs = new FileSystem();
			fs.Mount("/", findResources());
			fs.DefaultContext.AddResources(new TextResource(), new GLSLShaderResource(), new ColladaResource());
			fs.DefaultContext["GraphicsDevice"] = r;

			var vertexShader = fs.DefaultContext.ReadResource<VertexShader>("/Shaders/GLSL/Vertex/MVPTransform.glsl");
			var fragmentShader = fs.DefaultContext.ReadResource<FragmentShader>("/Shaders/GLSL/Fragment/BufferColored.glsl");

			fs.DefaultContext["VertexShader"] = vertexShader;
			fs.DefaultContext["FragmentShader"] = fragmentShader;

			fs.DefaultContext["Window"] = window;

			Iridescence.Graphics.Buffer sceneBuffer = r.CreateBuffer(128);
			Iridescence.Graphics.Buffer colorBuffer = r.CreateBuffer(16);

			Model model = fs.DefaultContext.ReadResource<Model>("/Models/box.dae");

			Matrix4x4 projMatrix = Matrix4x4.CreatePerspectiveFOV(Utility.DegreesToRadians(90.0f), window.Aspect, 0.01f, 100.0f);
			sceneBuffer.Write(0, ref projMatrix);

			Matrix4x4 viewMatrix = Matrix4x4.CreateTranslation(0f, 0f, -3f);
			sceneBuffer.Write(0, ref viewMatrix);

			colorBuffer.Write(new[]
			{
				0.0f, 1.0f, 0.0f, 1.0f,
			});

			var cmdList = new List<Command>();
			cmdList.SetRenderTarget(window);
			cmdList.SetViewports(window.GetViewport());

			cmdList.SetBuffer("Scene", sceneBuffer);
			cmdList.SetBuffer("Color", colorBuffer);
								
			cmdList.AddRange(model.CreateDrawCommand(r));

			var cmdBuffer = r.CreateCommandBuffer(cmdList);
			var clrBuffer = r.CreateCommandBuffer(new ClearCommand(window, new ClearParameters(ClearFlags.All, 0.11f, 0.12f, 0.13f, 1.0f, 1.0f, 0)));

			while (running)
			{
				r.Execute(clrBuffer);
				r.Execute(cmdBuffer);
				window.SwapBuffers();
			}
		}
	}
}
