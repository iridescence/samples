﻿using System.Collections.Generic;

using Iridescence.Graphics;
using Iridescence.Math;

namespace ColladaDemo
{
	public class Model : List<Mesh>
	{
		private Matrix4x4 modelMatrix;
		private Buffer modelBuffer;

		public Matrix4x4 ModelMatrix
		{
			get { return this.modelMatrix; }
			set 
			{ 
				this.modelMatrix = value;
				if (this.modelBuffer != null)
					this.modelBuffer.Write(0, ref this.modelMatrix);
			}
		}
		public Model()
		{
			this.modelMatrix = Matrix4x4.Identity;
		}

		public IEnumerable<Command> CreateDrawCommand(Device device)
		{
			this.modelBuffer = device.CreateBuffer(64);
			this.modelBuffer.Write(0, ref this.modelMatrix);

			var cmdList = new List<Command>();

			cmdList.SetBuffer("Model", modelBuffer);
			foreach(Mesh mesh in this)
				cmdList.AddRange(mesh.DrawCommand);
			
			return cmdList;
		}
	}
}

